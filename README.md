# Flask Blog

This project is used only for learning Flask purpouses, and there are some features to learn backend development. With those features is expected to learn some concepts of backend development with flask.
This project is taken from the [Corey Schaffer](https://www.youtube.com/watch?v=MwZwr5Tvyxo&list=PL-osiE80TeTs4UjLw5MM6OjgkjFeUxCYH)'s course on YouTube.

## List of features used in the blog

* Starting with Flask
* Templates
* Forms and Validations
* Database
* Package Structure*
* User Login System
* Profile Info and Picture
* Create, Update & Delete Posts
* Pagination
* Email & Password Reset
* Blueprint & Configuration
* Custom Error Pages
* Deployment in Linux Server
* Custom Domain

*Currently in this topic* (*)

## Things learned or viewed up to now


### Starting with Flask:
* How to setup the virtual environment
* How to install Flask in a project
* The route system in Flask
* How to activate de Debug Mode

### Templates
* Working with Jinja2 and Bootstrap
* Create a template
* Ifelse conditional
* Template inheritance
* Block Content
* Layout Template

### Database
* How to structure the database in the file
* How to call information from database

### Package Structure
* Restructure the project to work from models to packages
* And explained the circular import Traceback*

*A bit confusing topic* (*)
